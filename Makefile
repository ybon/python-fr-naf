develop:
	pip install -e .[dev,test]
fetch:  # Fetch data from INSEE
	wget https://www.insee.fr/fr/statistiques/fichier/2120875/int_courts_naf_rev_2.xls -O data.xls
update-db: SHELL := python3
update-db:
	import naf; naf.update_db("data.xls")
test:
	py.test -vvx
dist: clean
	python setup.py sdist bdist_wheel
upload:
	twine upload dist/*
clean:
	rm -rf *.egg-info/ dist/ build/
