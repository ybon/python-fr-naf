import pytest

from naf import DB, NAF


def test_get_naf_from_code():
    naf = DB["20.51Z"]
    assert naf.description == "Fabrication de produits explosifs"
    assert str(naf) == "Fabrication de produits explosifs"
    assert "20.51Z" in DB


def test_wrong_code():
    assert "12.34Q" not in DB
    with pytest.raises(KeyError):
        DB["12.34Q"]


def test_iter_db():
    for naf in DB:
        assert isinstance(naf, NAF)


def test_iter_db_paris():
    for code, description in DB.pairs():
        assert len(code) == 6
        assert code in DB
        assert DB[code].description == description


def test_categories():
    naf = DB["20.51Z"]
    assert naf.section.code == "C"
    assert naf.section == "C"
    assert str(naf.section) == "INDUSTRIE MANUFACTURIÈRE"
    assert naf.section.description == "INDUSTRIE MANUFACTURIÈRE"
    assert str(naf.division) == "Industrie chimique"
    assert naf.division == "20"
    assert str(naf.groupe) == "Fabrication d'autres produits chimiques"
    assert naf.groupe == "20.5"
    assert str(naf.classe) == "Fabrication de produits explosifs"
    assert naf.classe == "20.51"


def test_naf_as_dict():
    naf = DB["20.51Z"]
    assert dict(naf) == {
        "classe": {"code": "20.51", "description": "Fabrication de produits explosifs"},
        "code": "20.51Z",
        "description": "Fabrication de produits explosifs",
        "division": {"code": "20", "description": "Industrie chimique"},
        "groupe": {
            "code": "20.5",
            "description": "Fabrication d'autres produits chimiques",
        },
        "section": {"code": "C", "description": "INDUSTRIE MANUFACTURIÈRE"},
    }


def test_db_as_json():
    dumped = str(DB)
    assert isinstance(dumped, str)


def test_db_section():
    assert DB.section == {
        "A": "AGRICULTURE, SYLVICULTURE ET PÊCHE",
        "B": "INDUSTRIES EXTRACTIVES",
        "C": "INDUSTRIE MANUFACTURIÈRE",
        "D": "PRODUCTION ET DISTRIBUTION D'ÉLECTRICITÉ, DE GAZ, DE VAPEUR ET D'AIR "
        "CONDITIONNÉ",
        "E": "PRODUCTION ET DISTRIBUTION D'EAU ; ASSAINISSEMENT, GESTION DES DÉCHETS "
        "ET DÉPOLLUTION",
        "F": "CONSTRUCTION",
        "G": "COMMERCE ; RÉPARATION D'AUTOMOBILES ET DE MOTOCYCLES",
        "H": "TRANSPORTS ET ENTREPOSAGE",
        "I": "HÉBERGEMENT ET RESTAURATION",
        "J": "INFORMATION ET COMMUNICATION",
        "K": "ACTIVITÉS FINANCIÈRES ET D'ASSURANCE",
        "L": "ACTIVITÉS IMMOBILIÈRES",
        "M": "ACTIVITÉS SPÉCIALISÉES, SCIENTIFIQUES ET TECHNIQUES",
        "N": "ACTIVITÉS DE SERVICES ADMINISTRATIFS ET DE SOUTIEN",
        "O": "ADMINISTRATION PUBLIQUE ",
        "P": "ENSEIGNEMENT",
        "Q": "SANTÉ HUMAINE ET ACTION SOCIALE",
        "R": "ARTS, SPECTACLES ET ACTIVITÉS RÉCRÉATIVES",
        "S": "AUTRES ACTIVITÉS DE SERVICES",
        "T": "ACTIVITÉS DES MÉNAGES EN TANT QU'EMPLOYEURS ; ACTIVITÉS INDIFFÉRENCIÉES "
        "DES MÉNAGES EN TANT QUE PRODUCTEURS DE BIENS ET SERVICES POUR USAGE PROPRE",
        "U": "ACTIVITÉS EXTRA-TERRITORIALES",
    }
